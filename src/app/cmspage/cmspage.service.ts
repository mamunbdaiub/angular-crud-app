import {Injectable} from '@angular/core';
import {catchError} from "rxjs/internal/operators";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {throwError} from "rxjs/index";
import {Page} from "./page";

@Injectable({
    providedIn: 'root'
})
export class CmspageService {
    serverUrl = "http://localhost/dev/blogger/";
    errorData: {};

    constructor(private http: HttpClient) {
    }

    getPages(slug: string) {
        return this.http.get<Page>(this.serverUrl + "api/page").pipe(
            catchError(this.handlerData)
        );
    }

    private handlerData(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error("An error occurred:", error.error.message);
        } else {
            console.error(`Backend returned code ${error.status},` + `body was ${error.error}`)
        }

        this.errorData = {
            errorTitle: "Oops! Request for document failed",
            errorDesc: "Something bad happened. Please try again later."
        };

        return throwError(this.errorData)
    }
}
