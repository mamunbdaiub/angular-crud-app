import {Component, OnInit} from '@angular/core';
import {CmspageService} from "../cmspage.service";
import {Page} from "../page";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {switchMap} from "rxjs/internal/operators";

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
    page: Page;
    error: {};

    constructor(private cmsPageService: CmspageService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.paramMap.pipe(
            switchMap((params: ParamMap) => this.cmsPageService.getPages(params.get('slug')))
        ).subscribe(
            (data: Page) => this.page = data,
            error => this.error = error
        );
    }

}
