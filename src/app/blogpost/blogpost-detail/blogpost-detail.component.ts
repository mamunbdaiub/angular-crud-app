import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs/index";
import {Blogpost} from "../blogpost";
import {Title} from "@angular/platform-browser";
import {BlogpostService} from "../blogpost.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {switchMap} from "rxjs/internal/operators";

@Component({
    selector: 'app-blogpost-detail',
    templateUrl: './blogpost-detail.component.html',
    styleUrls: ['./blogpost-detail.component.css']
})
export class BlogpostDetailComponent implements OnInit {

    blog: Observable<Blogpost>;

    constructor(private titleService: Title,
                private blogPostService: BlogpostService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.blog = this.route.paramMap.pipe(
            switchMap((params: ParamMap) =>
                this.blogPostService.getBlog(params.get("id"))
            )
        );

        this.titleService.setTitle("Blog Details");
    }
}
