import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Blogpost} from "./blogpost";
import {catchError} from "rxjs/internal/operators";
import {throwError} from "rxjs";
import {error} from "util";

@Injectable({
    providedIn: 'root'
})
export class BlogpostService {
    serverUrl = "http://localhost:4200/";
    errorData: {};
    blogs: {};

    constructor(private http: HttpClient) {
    }

    getBlogs() {
        return this.http.get<Blogpost>(this.serverUrl + 'api/blog').pipe(
            catchError(this.handleError)
        )
    }

    getBlog(id: string) {
        return this.http.get<Blogpost>(this.serverUrl + 'api/blog').pipe(
            catchError(this.handleError)
        )
    }

    getFeaturedBlogs() {
        return this.http.get<Blogpost>(this.serverUrl + 'api/featured_blogs').pipe(
            catchError(this.handleError)
        );
    }

    getRecentBlogs() {
        return this.http.get<Blogpost>(this.serverUrl + "api/recent_blogs").pipe(
            catchError(this.handleError)
        );
    }

    getCategories() {
        return this.http.get(this.serverUrl + "api/categories").pipe(
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error("An error occurred:", error.error.message);
        } else {
            console.error(`Backend returned code ${error.status},` + ` body was: ${error.error}`)
        }

        this.errorData = {
            errorTitle: 'Oops! Request for document failed',
            errorDesc: "Something bad happened. Please try again later."
        };

        return throwError(this.errorData);
    }
}
