import { Component, OnInit } from '@angular/core';
import {Blogpost} from "../blogpost";
import {BlogpostService} from "../blogpost.service";

@Component({
  selector: 'app-blogpost-recent',
  templateUrl: './blogpost-recent.component.html',
  styleUrls: ['./blogpost-recent.component.css']
})
export class BlogpostRecentComponent implements OnInit {

  blogs: Blogpost;
  error: {};

  constructor(private blogPostService: BlogpostService) { }

  ngOnInit() {
    this.blogPostService.getRecentBlogs().subscribe(
        (data: Blogpost) => this.blogs = data, error => this.error = error
    );
  }
}
