import {Component, OnInit} from '@angular/core';
import {BlogpostService} from "../blogpost.service";
import {Category} from "../category";

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
    categories: Category;
    selectedCategory: Category;

    constructor(private blogPostService: BlogpostService) {
    }

    ngOnInit() {
        this.blogPostService.getCategories().subscribe(
            (data: Category) => this.categories = data
        );
    }

    onSelect(category: Category): void {
        this.selectedCategory = category;
    }
}
